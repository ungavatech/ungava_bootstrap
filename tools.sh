#!/bin/sh
####################################################################################################
##
## Copyright (C) UNGAVA Technologies Inc. All rights reserved.
##
## The code contained in this file is strictly CONFIDENTIAL and shall not be PROPAGATED BY ANY
## MEANS UNLESS OTHERWISE AUTHORIZED BY UNGAVA Technologies Inc.
##
####################################################################################################

getMyReposLastRepositoryName() {
  mr list 2> /dev/null | grep 'mr list' | tail -n 2 | head -n 1 | tr '/' '\n' | tail -n 1
}

getMyReposMaxOrder() {
  mr config "$(getMyReposLastRepositoryName)" order 2> /dev/null || echo 0
}

isMyReposRepositoryAlreadyRegistered() {
  local repoName="$1"
  # Check that to repo is registered by looking for its mandatory "checkout" attribute. 
  if 2>/dev/null 1>/dev/null mr config "$repoName" checkout; then
    return 0 # True
  else
    return 1 # False
  fi
}

registerMyReposRepositoryCmd() {
  local repositoryName=$1
  local wsDir=$2

  local repositoryDir="$wsDir/$repositoryName"

  if [ -d "$repositoryDir" ]; then
    local OLDPWD=$PWD; cd $repositoryDir
    mr register
    local errorCode=$?
    cd $OLDPWD
    return ${errorCode}
  else
    return 1 # Error
  fi
}

setMyReposRepositoryOrder() {
  local repoName="$1"

  local order=0

  if [ -n "$2" ]; then
    order="$2"
  elif [ -n "$BOOTSTRAP_MYREPOS_CURRENT_ORDER" ]; then
    order=$BOOTSTRAP_MYREPOS_CURRENT_ORDER
    export BOOTSTRAP_MYREPOS_CURRENT_ORDER=`expr 1 + ${order}`
  fi

  mr config "${repoName}" order=${order}
}


initMyRepoWorkspace () {
    local workspaceDir=$1

    local OLDPWD=$PWD; cd $workspaceDir

    # When the `mr`command is found Initialize the *myrepo* `.mrconfig` configuration file so that
    # repositories will be registered there.
    if command -v mr > /dev/null; then
      if [ -f "$workspaceDir/.mrconfig" ]; then
        # Start further ordering from the maximal order found in already registered repositories.
        local maxOrder=$(getMyReposMaxOrder)
        export BOOTSTRAP_MYREPOS_CURRENT_ORDER=`expr 1 + ${maxOrder}`
      else
        touch $workspaceDir/.mrconfig
        export BOOTSTRAP_MYREPOS_CURRENT_ORDER=0
      fi
    fi

    cd $OLDPWD
}

registerMyRepoRepository () {
    local repositoryName=$1
    local wsDir=$2
    local logFile=$3

    local errorCode=0

    local OLDPWD=$PWD; cd $wsDir

    if command -v mr > /dev/null; then
      isMyReposRepositoryAlreadyRegistered "$repositoryName"
      alreadyRegisteredCode=$?

      registerMyReposRepositoryCmd "$repositoryName" "$wsDir" >> $logFile && \
      { test 0 -eq $alreadyRegisteredCode || \
        setMyReposRepositoryOrder "$repositoryName" >> $logFile; }

      errorCode=$?
    fi

    cd $OLDPWD

    printf "\n" >> $logFile

    return ${errorCode}
}

registerGitHooks () {
    local repoName=$1
    local repoDir=$2/$1
    local deployHooksScript=$BOOTSTRAP_SCRIPT_DIR/deploy_git_hooks.sh
    local logFile=$3
    
    if [ ! -e "$deployHooksScript" ]; then
      echo "Could not find git hook deploy script at expected location:" | tee -a $logFile
      echo "\"${deployHooksScript}\"" | tee -a $logFile
      return 1
    fi

    $deployHooksScript $repoName | tee -a $logFile
    return $?
}

printBootrapLogHeader() {
    local projectName=$1
    local projectDir=$2
    local logFile=$3

    printf "BOOTSTRAP LOG for $projectName at $projectDir\n\n\n" > $logFile
}

createSolutionProject() {
    local projectName=$1
    local workspaceDir=$2
    local relativePathToProjectDir=$3

    # Create a `$projectName.pro` solution project in worspace directory (one level above the project's
    # repository  root and other repositories) which will point to the project internal `env/solution.pro`.
    # This make it possible for the project and all its dependencies to build as part of a single build context.
    echo "TEMPLATE = subdirs" > $workspaceDir/$projectName.pro
    echo "SUBDIRS += ./$relativePathToProjectDir/env/solution.pro" >> \
        $workspaceDir/$projectName.pro
}

beginBootstrapSequence () {
    local projectName=$1
    local workspaceDir=$2
    local relativePathToProjectDir=$3

    local projectDir=$workspaceDir/$relativePathToProjectDir

    local logFile=$workspaceDir/bootstrap.log

    printBootrapLogHeader \
        "$projectName" "$projectDir" "$logFile"

    createSolutionProject \
        "$projectName" "$workspaceDir" "$relativePathToProjectDir"
    initMyRepoWorkspace "$workspaceDir"

    export BOOTSTRAP_PROJECT_NAME=$projectName
    export BOOTSTRAP_SCRIPT_DIR=$workspaceDir/ungava_bootstrap
    export BOOTSTRAP_WORKSPACE_DIR=$workspaceDir
    export BOOTSTRAP_LOG_FILE=$workspaceDir/bootstrap.log

    if [ ! -d "$BOOTSTRAP_SCRIPT_DIR" ] || [ ! -d "$BOOTSTRAP_SCRIPT_DIR/bin"  ]; then
      echo "ERROR: \`ungava_bootstrap\` could not be found at expected location or is incomplete:"
      echo "\"$BOOTSTRAP_SCRIPT_DIR\""
      echo "Could not proceed with the bootstrap process."
      exit 1
    fi

    # As a convenience, automatically register the current bootstrap repository
    # with myrepo (\`mr\` command).
    registerMyRepoRepository \
        "ungava_bootstrap" "$workspaceDir" "$logFile"

    # Bring "env.h" and "bin" from "ungava_bootrap" to the current workspace.
    mkdir -p $BOOTSTRAP_WORKSPACE_DIR/bin
    rm -f $BOOTSTRAP_WORKSPACE_DIR/bin/*
    cp -pf -t $BOOTSTRAP_WORKSPACE_DIR/bin $BOOTSTRAP_SCRIPT_DIR/bin/*
    cp -pf -t $BOOTSTRAP_WORKSPACE_DIR $BOOTSTRAP_SCRIPT_DIR/env.sh
}

endBootstrapSequence () {
    local logFile=$BOOTSTRAP_LOG_FILE

}

registerRepository () {
    local repositoryName=$1
    local workspaceDir=$BOOTSTRAP_WORKSPACE_DIR
    local logFile=$BOOTSTRAP_LOG_FILE

    registerMyRepoRepository \
        "$repositoryName" "$workspaceDir" "$logFile"
    
    local registerMyRepoStatus=$?
    
    registerGitHooks \
	"$repositoryName" "$workspaceDir" "$logFile"

    local registerGitHooksStatus=$?

    if [ 0 -ne $registerMyRepoStatus ] || [ 0 -ne $registerGitHooksStatus ]; then
	return 1
    fi
}

cloneRepository () {
    local REPOSITORY_NAME=$1
    local REPOSITORY_TARGET_DIR=$2
    local REPOSITORY_URL=$3
    local VCS_COMMAND="$4"
    local LOG_FILE=$5
    local LOCAL_NAME=$6

    local REPOSITORY_LOCAL_NAME=$REPOSITORY_NAME
    if [ "" != "$LOCAL_NAME" ]; then
        REPOSITORY_LOCAL_NAME=$LOCAL_NAME
    fi

    local VCS_ERROR_CODE=0

    if [ -e "$REPOSITORY_TARGET_DIR/$REPOSITORY_LOCAL_NAME" ]; then
        echo "\"$REPOSITORY_LOCAL_NAME\" already exists in target dir \"$REPOSITORY_TARGET_DIR\"." \
            | tee -a $LOG_FILE
        echo "    You should make sure it is up to date." \
            | tee -a $LOG_FILE
    else
        echo "Cloning \"$REPOSITORY_URL\" into \"$REPOSITORY_TARGET_DIR/$REPOSITORY_LOCAL_NAME\"." \
            | tee -a $LOG_FILE
        local OLDPWD=$PWD; cd $REPOSITORY_TARGET_DIR

        local tmpCmdErrorLog=${LOG_FILE}_tmp
        ${VCS_COMMAND} $REPOSITORY_URL $LOCAL_NAME >$tmpCmdErrorLog 2>&1
        VCS_ERROR_CODE=$?
        cat $tmpCmdErrorLog | sed -e 's/^/    /' >> $LOG_FILE
        rm -f $tmpCmdErrorLog

        if [ $VCS_ERROR_CODE -ne 0 ]; then
            echo "    Exited with error code: $VCS_ERROR_CODE. See $LOG_FILE for more details." \
                | tee -a $LOG_FILE
        else
            echo "    Success." \
                | tee -a $LOG_FILE
        fi
        cd $OLDPWD
    fi
    printf "\n" >> $LOG_FILE

    return $VCS_ERROR_CODE
}

cloneGitRepository () {
    local workspaceDir=$BOOTSTRAP_WORKSPACE_DIR
    local logFile=$BOOTSTRAP_LOG_FILE

    local repoUrl=$2
    local repoRev=$3
    # TODO: Do something with rev. Currently unused.
    
    cloneRepository "$1" "$workspaceDir" "${repoUrl}" "git clone" "$logFile"
    local cloneErrorCode=$?

    [ $cloneErrorCode -eq 0 ] && registerMyRepoRepository "$1" "$workspaceDir" "$logFile"
    [ $cloneErrorCode -eq 0 ] && registerGitHooks "$1" "$workspaceDir" "$logFile"
    
    return $cloneErrorCode
}


checkoutSvnRepository () {
    local workspaceDir=$BOOTSTRAP_WORKSPACE_DIR
    local logFile=$BOOTSTRAP_LOG_FILE

    local repoUrl=$2
    local repoRev=$3
    # TODO: Do something with rev. Currently unused.
    
    cloneRepository "$1" "$workspaceDir" "${repoUrl}" "svn co"\
        "$logFile" "$1"

    local cloneErrorCode=$?
    return $cloneErrorCode
}

checkoutAnyTypeOfRepository () {
    local repoName=$1
    local repoType=$2
    local repoUrl=$3
    local repoRev=$4

    if [ "git" = "$repoType" ]; then
	cloneGitRepository "${repoName}" "${repoUrl}" "${repoRev}"
	return $?
    elif [ "svn" = "$repoType" ]; then
	checkoutSvnRepository "${repoName}" "${repoUrl}" "${repoRev}"
	return $?
    else
	echo "ERROR: Repository \"${repoName}\" has unsupported type \"${repoType}\". Unable to checkout."
	return 1
    fi
}

cloneBitbucketGitRepo () {
    local teamName="$1"
    local repoName="$2"
    local UNGAVA_URL_PREFIX="https://bitbucket.org/${teamName}"
    cloneGitRepository "$repoName" "$UNGAVA_URL_PREFIX/${repoName}.git"
    return $?
}

cloneUngavaTechBitbucketGitRepo () {
    local repoName="$1"
    cloneBitbucketGitRepo "ungavatech" "$repoName"
}

checkoutUngavaSoftwareSvnRepo () {
    local workspaceDir=$BOOTSTRAP_WORKSPACE_DIR
    local logFile=$BOOTSTRAP_LOG_FILE

    local UNGAVA_URL_PREFIX=svn://192.168.0.3/ungava/software
    cloneRepository "$1" "$workspaceDir" "${UNGAVA_URL_PREFIX}${2}/${1}" "svn co"\
        "$logFile" ""

    local cloneErrorCode=$?
    return $cloneErrorCode
}

checkoutUngavaSoftwareSvnRepoTo () {
    local workspaceDir=$BOOTSTRAP_WORKSPACE_DIR
    local logFile=$BOOTSTRAP_LOG_FILE
    local localName=$2

    local UNGAVA_URL_PREFIX=svn://192.168.0.3/ungava/software
    cloneRepository "$1" "$workspaceDir" "${UNGAVA_URL_PREFIX}${3}/${1}" "svn co"\
        "$logFile" "$localName"

    local cloneErrorCode=$?
    return $cloneErrorCode
}

cloneUngavaTechInternalRepo () {
    local workspaceDir=$BOOTSTRAP_WORKSPACE_DIR
    local logFile=$BOOTSTRAP_LOG_FILE
    local repoLocalName=$1

    if [ "ungava_internal" = "$repoLocalName" ]; then
        checkoutUngavaSoftwareSvnRepoTo "trunk" "$repoLocalName"
        local cloneErrorCode=$?
        [ $cloneErrorCode -eq 0 ] && registerMyRepoRepository "$repoLocalName" "$workspaceDir" "$logFile"
        return $cloneErrorCode
    else
        echo "    No internal repository with name \"ungava_internal\" exists." \
            | tee -a $LOG_FILE
        return 1
    fi
} 

