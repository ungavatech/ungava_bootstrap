#!/bin/sh
####################################################################################################
##
## Copyright (C) UNGAVA Technologies Inc. All rights reserved.
##
## The code contained in this file is strictly CONFIDENTIAL and shall not be PROPAGATED BY ANY
## MEANS UNLESS OTHERWISE AUTHORIZED BY UNGAVA Technologies Inc.
##
####################################################################################################
####################################################################################################
## @module env.sh
## @brief Set the proper development environment for a workspace.
##
##  This alongside with the its companion `bin` directory are meant to be copied to
##  the user's workspace (root location) by any boostrap script so that user is provided with
##  an easy mean of entering a environment with all utility scripts in `PATH`.
##
##  IMPORTANT: This script is to be **sourced** from the user's workspace or from the boostrap
##  repository (in case user has not yet called any boostrap. 
##             
##  From the workspace:
##
##  ~~~
##  cd $MY_PROJECT_WORKSPACE; . env.sh
##  ~~~
##
##  or, using bash:
##
##  ~~~
##  cd $MY_PROJECT_WORKSPACE; source env.sh
##  ~~~
##
##  From the boostrap repository:
##
##  ~~~
##  cd $MY_BOOTSTRAP_REPO_DIR; . env.sh
##  ~~~
##
####################################################################################################


OLDPWD=$PWD; cd `dirname "$0"`
UNGAVA_BOOTSTRAP_BIN_DIR=$PWD/bin
cd $OLDPWD


PATH=$PATH:$UNGAVA_BOOTSTRAP_BIN_DIR
export PATH


