#!/bin/sh
####################################################################################################
##
## Copyright (C) UNGAVA Technologies Inc. All rights reserved.
##
## The code contained in this file is strictly CONFIDENTIAL and shall not be PROPAGATED BY ANY
## MEANS UNLESS OTHERWISE AUTHORIZED BY UNGAVA Technologies Inc.
##
####################################################################################################

OLDPWD=$PWD; cd `dirname "$0"`
SCRIPT_DIR=$PWD
cd $OLDPWD

OLDPWD=$PWD; cd $SCRIPT_DIR/..
WS_DIR=$PWD
cd $OLDPWD

REPO_NAME=$1
REPO_DIR=$WS_DIR/$REPO_NAME

if [ "$REPO_NAME" = "" ] || [ ! -d "$REPO_DIR" ]; then
    echo "ERROR: A valid repository name should be provided as argument."
    exit 1
fi

BOOTSTRAP_DIR=$SCRIPT_DIR
HOOKS_SRC_DIR=$SCRIPT_DIR/git_hooks
HOOKS_TGT_DIR=$REPO_DIR/.git/hooks

if [ ! -d "$HOOKS_TGT_DIR" ]; then
    echo "ERROR: Repository \"${REPO_DIR}\" does not include expected \"./.git/hooks\" sub directory."
    exit 1
fi

# Copy the actual hooks.
cp -lfrp --target-directory=$HOOKS_TGT_DIR $HOOKS_SRC_DIR/*

# Copy individual utilities required by hooks.
mkdir -p $HOOKS_TGT_DIR/bin
OLDPWD=$PWD; cd $BOOTSTRAP_DIR
cp -lfp --target-directory=$HOOKS_TGT_DIR/bin ./bin/dependencies_snapshot
cd $OLDPWD
